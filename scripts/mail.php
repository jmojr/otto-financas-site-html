<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../phpmailer/vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);
$nome		= $_POST["name"];
$email		= $_POST["email-contact"];
$mensagem	= $_POST["message"];
$conteudo 		= "Nome: $nome\n\nE-mail: $email\n\nMensagem: $mensagem\n";

try {
    //Server settings
    $mail->isSMTP();
    $mail->Host       = 'smtp.hostinger.com';
    $mail->SMTPAuth   = true;
    $mail->Username   = 'contato@ottoassistente.com';
    $mail->Password   = 'assistente@1Otto';
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port       = 587;
    $mail->CharSet = 'utf-8';
    //Recipients
    $mail->setFrom('contato@ottoassistente.com', $nome);
    $mail->addAddress('contato@ottoassistente.com');

    // Content
    $mail->isHTML(true);
    $mail->Subject = 'Email de contato - Otto Finanças - Site';
    $mail->Body    = $conteudo;
    $mail->AltBody = $conteudo;

    $mail->send();
    header('location: http://localhost/otto/public/index.html#contact-form');
} catch (Exception $e) {
    echo "Mensagem não enviada. Mailer Error: {$mail->ErrorInfo}";
}